<!-- <img style="display: block; text-align: center"
     src="https://media.geeksforgeeks.org/wp-content/uploads/20221123153249/SkillsRequiredtoBecomeaBackendDeveloper.png"> -->
<h1 align="center">Hi 👋, I'm Khasanjon 👨🏻‍💻</h1>

[![Telegram Profile](https://img.shields.io/badge/-telegram-blueviolet?style=for-the-badge&logo=telegram)][telegram]
[![LinkedIn Profile](https://img.shields.io/badge/-linkedin-brightgreen?style=for-the-badge&logo=linkedin)][linkedin]
[![Instagram Profile](https://img.shields.io/badge/-instagram-blue?style=for-the-badge&logo=instagram)][instagram]

<h3 align="center">⚡ A Young Backend Developer ⚡</h3>

- 🔭 I’m currently working as a **Backend Developer** 🔴

- 💠 I have **3+ years** of experience in programming! 🟣

- ☁️ On the cloud side, I know **AWS ( EC2 ) & HEROKU** 🔵

- ⚙️ I’m currently learning **DRF, Web3 Development & DESIGN PATTERN** 🟠

- 👨‍💻 All of my projects are available at my **GitHub**! 🟢

- ⚡ Fun fact about me: **I keep learning!** 🔴

- 💬 Ask me about **Backend Architecture | DEVOPS** 🔵


<h3 align="center">MY TECH STACK</h3>
<p align="center">
    <a href="https://github.com/khasanjon-mamadaliyev">
        <img src="https://skillicons.dev/icons?i=linux,python,fastapi,django,flask,docker,nginx,postgres,redis,rabbitmq,postman,heroku,aws"/>
    </a>
</p>
<p align="center">
    <a href="https://github.com/khasanjon-mamadaliyev">
        <img src="https://skillicons.dev/icons?i=git,github,githubactions,gitlab,sqlite,vim,c,bash"/>
    </a>
</p>


[telegram]: https://t.me//khasanjon_eng
[instagram]: https://www.instagram.com/khasanjon_mamadaliyev
[linkedin]: https://www.linkedin.com/in/khasanjon-mamadaliyev
